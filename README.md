# README #

Repositorio do servidor e criacao de dataset informativo com historico de vitorias e mares de vitorias.


para correr o servidor é necessario fazer:           
         
pip install flask      
pip install -U flask-cors           
pip install sklearn    
conda install -c conda-forge flask-cors        
         
e depois correr o ficheiro servidor.py

NAO funciona devido a um bug recente em Windows. Bug nao existe em linux:            
https://github.com/numpy/numpy/issues/16744          
         
MAS o prompt do Python Anaconda consegue correr:             
python servidor.py        


Exemplo de um pedido:           
http://127.0.0.1:5000/?home=Sheffield United&away=Wolves
         
Resposta:            
Vitoria do equipa visitante, Wolves com Over 2.5 golos.          
