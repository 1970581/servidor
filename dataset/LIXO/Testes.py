from liga import Liga
from geraClubes import Gerador
import glob
import pandas


#liga = Liga("PORT",20)

#print liga.contadorPontos["20"]
cont=0
listGer=[]
lfich=[]
for filename in glob.glob('./Dados/*.csv'):
	lfich.append((filename.split(".csv"))[0].split("./Dados\\")[1])

lfich= [int(x) for x in lfich]
 
for filename in sorted(lfich, key=int):
	filename="./Dados/"+str(filename)+".csv"
	if cont==0:
		fich=Gerador(filename,"premier","15/08/16","15/05/17")
		fich.printa()
		listGer.append(fich)
		print "CLUBES LIGA: \n"
		print fich.listaClubes	
		print "PRIM: ",fich.dicKeyNumJogos
		cont+=1
	else:
		fich=Gerador(filename,"premier","15/08/16","15/05/17")
		gerAnt=listGer[len(listGer)-1]
		fich.dicVitoria=gerAnt.dicVitoria
		fich.dicVitoriaCasa=gerAnt.dicVitoriaCasa
		fich.dicVitoriaFora=gerAnt.dicVitoriaFora
		fich.listaClubes=gerAnt.listaClubes	
		fich.contClubles=gerAnt.contClubles
		fich.dicKeyNumJogos=gerAnt.dicKeyNumJogos
		fich.dicKeyNumJogosCasa=gerAnt.dicKeyNumJogosCasa
		fich.dicKeyNumJogosFora=gerAnt.dicKeyNumJogosFora
		fich.btw=gerAnt.calcula.calculadorEntreEquipas
		print"VALOR ARRAY ANTIGO ",gerAnt.arratBTW
		fich.arratBTW=gerAnt.arratBTW
		print"VALOR ARRAY AGORA ",fich.arratBTW
		fich.dicNomeByKey=gerAnt.dicNomeByKey
		fich.printa()
		listGer.append(fich)
		print "CLUBES LIGA: \n"
		print fich.listaClubes


Equipa1=[]
Equipa2=[]
EVitoriaCasa=[]
EVitoriaFora=[]
EVitoriaEntreE=[]
EPontos=[]
EPontosCasa=[]
Vitoria=[]
EGolos=[]
oddVitoria=[]
oddEmpate=[]
oddDerrota=[]
nomeCasa=[]
nomeFora=[]
jogosCasa=[]
jogosFora=[]

##prever##
Equipa1P=[]
Equipa2P=[]
EVitoriaCasaP=[]
EVitoriaForaP=[]
EVitoriaEntreEP=[]
EPontosP=[]
EPontosCasaP=[]
VitoriaP=[]
EGolosP=[]
nomeCasaP=[]
nomeForaP=[]
jogosCasaP=[]
jogosForaP=[]

for x in listGer:
	
	for j in range(len(x.games2["HomeTeam"].dropna(how='any'))):
		if x.games2["Vitoria"][j]=="nv":
			Equipa1P.append(x.games2["HomeTeam"][j])
			nomeCasaP.append(x.dicNomeByKey[x.games2["HomeTeam"][j]])
			nomeForaP.append(x.dicNomeByKey[x.games2["AwayTeam"][j]])
			#oddVitoria.append(x.games2["B365H"][j])
			#oddEmpate.append(x.games2["B365D"][j])
			#oddDerrota.append(x.games2["B365A"][j])
			Equipa2P.append(x.games2["AwayTeam"][j])
			VitoriaP.append(x.games2["Vitoria"][j])
			EVitoriaCasaP.append(x.calcula.calculadorVitoriaCasa[j])
			EVitoriaForaP.append(x.calcula.calculadorVitoriaFora[j])
			EVitoriaEntreEP.append(x.calcula.calculadorEntreEquipas[j])
			EPontosP.append(x.calcula.pontosFinal[j])
			EPontosCasaP.append(x.calcula.pontosFinalCasa_Fora[j])
			EGolosP.append(x.calcula.golosFinal[j])
			jogosCasaP.append(x.calcula.calculadorJogosCasa[j])
			jogosForaP.append(x.calcula.calculadorJogosFora[j])
		else:	
			Equipa1.append(x.games2["HomeTeam"][j])
			nomeCasa.append(x.dicNomeByKey[x.games2["HomeTeam"][j]])
			nomeFora.append(x.dicNomeByKey[x.games2["AwayTeam"][j]])
			#oddVitoria.append(x.games2["B365H"][j])
			#oddEmpate.append(x.games2["B365D"][j])
			#oddDerrota.append(x.games2["B365A"][j])
			Equipa2.append(x.games2["AwayTeam"][j])
			Vitoria.append(x.games2["Vitoria"][j])
			EVitoriaCasa.append(x.calcula.calculadorVitoriaCasa[j])
			EVitoriaFora.append(x.calcula.calculadorVitoriaFora[j])
			EVitoriaEntreE.append(x.calcula.calculadorEntreEquipas[j])
			EPontos.append(x.calcula.pontosFinal[j])
			EPontosCasa.append(x.calcula.pontosFinalCasa_Fora[j])
			EGolos.append(x.calcula.golosFinal[j])
			jogosCasa.append(x.calcula.calculadorJogosCasa[j])
			jogosFora.append(x.calcula.calculadorJogosFora[j])
		
	
d = {'HomeTeam' : pandas.Series(Equipa1),
	 'AwayTeam' : pandas.Series(Equipa2),
	 'team1' : pandas.Series(EVitoriaCasa),
  	 'team2': pandas.Series(EVitoriaFora),
  	 'gameBTW': pandas.Series(EVitoriaEntreE),
  	 'golo diff':pandas.Series(EPontos),
  	 'golo diff2':pandas.Series(EPontosCasa),
  	 'golo diff3':pandas.Series(EGolos),
  	 'JCasa':pandas.Series(jogosCasa),
  	 'JFora':pandas.Series(jogosFora),
  	 'oddVitoria':pandas.Series(oddVitoria),
  	 'oddEmpate':pandas.Series(oddEmpate),
  	 'oddDerrota':pandas.Series(oddDerrota),
  	 'ECasa':pandas.Series(nomeCasa),
  	 'EFora':pandas.Series(nomeFora),
 	 'vitoria' : pandas.Series(Vitoria),
	}
df= pandas.DataFrame(d)
df.to_csv("FF.csv", sep=',')

d = {'HomeTeam' : pandas.Series(Equipa1P),
	 'AwayTeam' : pandas.Series(Equipa2P),
	 'team1' : pandas.Series(EVitoriaCasaP),
  	 'team2': pandas.Series(EVitoriaForaP),
  	 'gameBTW': pandas.Series(EVitoriaEntreEP),
  	 'golo diff':pandas.Series(EPontosP),
  	 'golo diff2':pandas.Series(EPontosCasaP),
  	 'golo diff3':pandas.Series(EGolosP),
  	 'JCasa':pandas.Series(jogosCasaP),
  	 'JFora':pandas.Series(jogosForaP),
  	 'ECasa':pandas.Series(nomeCasaP),
  	 'EFora':pandas.Series(nomeForaP),
 	 'vitoria' : pandas.Series(VitoriaP),
	}
df= pandas.DataFrame(d)
df.to_csv("input.csv", sep=',')


# arrayClubes=[]
# arrayContadorPonto=[]
# arraytBTW=[]
# arrayContadorPontosCasa=[]
# arrayContadorPontosFora=[]
# arrayDicKeyNumJogos=[]
# arrayDicKeyNumJogosCasa=[]
# arrayDicKeyNumJogosFora=[]
# arrayDicVitoria=[]
# for x in range(len(fich.ligaT.contadorPontos)):
# 	if len(fich.ligaT.contadorPonto[x])>=0:
# 		arrayClubes.append(x)
# 		arrayContadorPonto.append(fich.ligaT.contadorPonto[x])

# 		arraytBTW.append(fich.arratBTW[x])
# 		arrayContadorPontosCasa.append(fich.ligaT.contadorPontosCasa[x])
# 		arrayContadorPontosFora.append(fich.ligaT.contadorPontosFora[x])
# 		arrayDicKeyNumJogos.append(fich.dicKeyNumJogos[x])
# 		arrayDicKeyNumJogosCasa.append(fich.dicKeyNumJogosCasa[x])
# 		arrayDicKeyNumJogosFora.append(fich.dicKeyNumJogosFora[x])
# 		arrayDicVitoria.append(ich.dicVitoria[x])

# d1 = {'EQUIPAS' : pandas.Series(arrayClubes),
# 	 'Pontos Totais' : pandas.Series(arrayContadorPonto),
# 	 'Entre Equipas': pandas.Series(arraytBTW),
# 	 'Pontos Casa' : pandas.Series(arrayContadorPontosCasa),
# 	 'Pontos Fora' : pandas.Series(arrayContadorPontosFora),	 
# 	 'Total Jogos' : pandas.Series(arrayDicKeyNumJogos),
# 	 'Total Jogos Casa' : pandas.Series(arrayDicKeyNumJogosCasa),
# 	 'Total Jogos Fora' : pandas.Series(arrayDicKeyNumJogosFora),
# 	 'Ultimos 5' : pandas.Series(arrayDicVitoria)
# 	}
# df2= pandas.DataFrame(d1)
# df2.to_csv("aux.csv", sep='\t')	
	
# #print fich.getValues("11","17")		

