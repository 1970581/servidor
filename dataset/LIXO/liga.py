

class Liga:
	def __init__(self, nome,nEquipas,equipas):
		self.nome=nome
	   # self.contadorPontos={"1":0,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0,
		#					"11":0,"12":0,"13":0,"14":0,"15":0,"16":0,"17":0,"18":0,"19":0,"20":0}
		self.contadorPontos={}
		self.contadorPontosCasa={}
		self.contadorPontosFora={}
		self.contadorGolos={}
		self.contadorGolosCasa={}
		self.contadorGolosFora={}
		self.contadorGolosSofridos={}
		self.nNumeroEquipas=nEquipas
		self.equipas=equipas
		#self.nClube=arrClube
		#self.contadorGolos={"1":0,"2":0,"3":0,"4":0,"5":0,"6":0,"7":0,"8":0,"9":0,"10":0,
		#					"11":0,"12":0,"13":0,"14":0,"15":0,"16":0,"17":0,"18":0,"19":0,"20":0}
		for x in range(1,nEquipas+1):
			key=str(x)
			self.contadorGolos[key]=0
			self.contadorPontos[key]=0
			self.contadorPontosCasa[key]=0
			self.contadorPontosFora[key]=0
			self.contadorGolos[key]=0
			self.contadorGolosCasa[key]=0
			self.contadorGolosFora[key]=0
			self.contadorGolosSofridos[key]=0


	def jogoPontos(self,equipaCasa,equipaFora,valor):
		if valor == "0":
			self.contadorPontos[equipaCasa]+=3
		if valor == "1":
			self.contadorPontos[equipaCasa]+=1
			self.contadorPontos[equipaFora]+=1
		if valor == "2":
			self.contadorPontos[equipaFora]+=3

	def jogoGolos(self,equipaCasa,equipaFora,golosCasa,golosFora):
		self.contadorGolos[equipaCasa]+=golosCasa
		self.contadorGolos[equipaFora]+=golosFora


