#coding=utf-8
from __future__ import division
import numpy as np
import pandas
import scipy.stats as stats
import matplotlib.pyplot as plt
import sklearn
from sklearn.datasets import load_boston
from liga import Liga
from CalculosX import Calculos

class Gerador:
    def __init__(self,nomeFicheiro,nomeLiga,dataInicio,dataFim):
        self.nomeFicheiro=nomeFicheiro
        self.games2 = pandas.read_csv(nomeFicheiro)
        self.columns = self.games2.columns.tolist()
        self.calcula=Calculos()
        self.dicVitoria={}
        self.dicVitoriaCasa={}
        self.dicVitoriaFora={}
        self.dicKeyByNome={}
        self.nLiga=nomeLiga
        self.ligaT=None
        self.dicKeyNumJogos={}
        self.dicKeyNumJogosCasa={}
        self.dicKeyNumJogosFora={}
        self.dicNomeByKey={}
        self.dataInicio=dataInicio
        self.dataFim=dataFim
        self.listaClubes=[]
        self.contClubles=[]
        self.contCl=0
        self.arratBTW={}


    def getData(self,data):
        dInicio=self.dataInicio.split("/")
        dFim=self.dataInicio.split("/")
        return True
    def geraClubes(self):

     listaClubesL=[]
     contClublesL=[]
     self.contCl=len(self.dicKeyNumJogos)+1
    # print len(self.dicKeyNumJogos)
     contClInit=self.contCl
     for cod in range(len(self.dicKeyNumJogos)):
         k=cod+1
         self.dicKeyNumJogos[str(k)]=0
         self.dicKeyNumJogosCasa[str(k)]=0
         self.dicKeyNumJogosFora[str(k)]=0
     #print "CLUBS INICIO",self.listaClubes
     #print "INICIO KEY ",self.dicKeyNumJogos
     print ("ANO", self.nomeFicheiro)


     for t in range(len(self.games2["HomeTeam"].dropna(how='any'))):
         #if self.getData(self.games2["Date"][t])==True:
             #ver ligas
             if self.games2["HomeTeam"][t] not in self.listaClubes:
                print ("NOVO ",self.games2["HomeTeam"][t])
                self.listaClubes.append(self.games2["HomeTeam"][t])
                #print "CICLO: ",self.listaClubes
                self.contClubles.append(str(self.contCl))
                #print "CICLO: ",self.contClubles
                self.dicNomeByKey[str(self.contCl)]=self.games2["HomeTeam"][t]
                self.games2["HomeTeam"][t]=str(self.contCl)
                self.dicKeyNumJogos[str(self.contCl)]=0
                self.dicKeyNumJogosCasa[str(self.contCl)]=0
                self.dicKeyNumJogosFora[str(self.contCl)]=0
                #print "CICLO: ",self.dicKeyNumJogos
                self.contCl+=1
                listaClubesL.append(self.games2["HomeTeam"][t])
                contClublesL.append(str(self.contCl))
            else:
                for x in range(len(self.listaClubes)):
                    if self.games2["HomeTeam"][t] == self.listaClubes[x]:
                        self.games2["HomeTeam"][t]=self.contClubles[x]
                        listaClubesL.append(self.listaClubes[x])
                        contClublesL.append(self.contClubles[x])

     for t in range(len(self.games2["AwayTeam"])):
        for x in range(len(self.listaClubes)):
            if self.games2["AwayTeam"][t] == self.listaClubes[x]:
                self.games2["AwayTeam"][t]=self.contClubles[x]
                listaClubesL.append(self.listaClubes[x])
                contClublesL.append(self.contClubles[x])
     for key in self.contClubles:
         if key not in self.dicVitoria.keys():
                self.dicVitoria[key]=[]
                self.dicVitoriaCasa[key]=[]
                self.dicVitoriaFora[key]=[]
     print ("VITORIAS ",self.dicVitoria)
     print ("EQUIPAS",self.dicNomeByKey)

     self.ligaT=Liga(self.nLiga,len(self.contClubles),self.listaClubes)

     for nome in self.listaClubes:
         self.dicKeyByNome[nome]=str(contClInit)
         contClInit+=1


    def geraVitoria(self):
        FTHG=[]
        FTAG=[]
        HTHG=[]
        HTAG=[]
        vF=[]
        golos=[]


        for t in range(len(self.games2["FTHG"])):
            if str(self.games2["FTHG"][t]).lower() != "nan":
                FTHG.append(self.games2["FTHG"][t])
                self.games2["FTHG"]
            else:
                FTHG.append("nan")


        for t in range(len(self.games2["FTAG"])):
            if str(self.games2["FTAG"][t]).lower() != "nan":
                FTAG.append(self.games2["FTAG"][t])
            else:
                FTAG.append("nan")
        for t in range(len(self.games2["HTHG"])):
            if str(self.games2["HTHG"][t]).lower() != "nan":
                HTHG.append(self.games2["HTHG"][t])
            else:
                HTHG.append("nan")
        for t in range(len(self.games2["HTAG"])):
            if str(self.games2["HTAG"][t]).lower() != "nan":
                HTAG.append(self.games2["HTAG"][t])
            else:
                HTAG.append("nan")

        for i in range(len(FTHG)):
            print (FTHG[i])
            print (HTHG[i])
            print (HTAG[i])
            print (FTAG[i])
            print i
            if FTHG[i] == "nan":
                vF.append("nv")
                golos.append(0)
            else:
                v1=int(FTHG[i])+int(HTHG[i])
                v2=int(FTAG[i])+int(HTAG[i])
                golos.append(v1+v2)
                if v1>v2:
                    vF.append("0")
                else:
                    if v1 == v2:
                        vF.append("1")
                    else:
                        if v1<v2:
                            vF.append("2")
        print ("VITORIA",len(vF))

        self.games2["Vitoria"]=vF

        self.games2["Total Golos"]=golos
        return vF,golos


    def calculaJC(self,teamKey):
        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            return (int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4]))/10
            #print "CASA"
            #print "Antes: ",self.dicVitoria[teamKey]

    def calculaJF(self,teamKey):
        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            return (int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4]))/10

    def calculaJogoCasa2(self,g,teamKey):

        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorVitoriaCasa.append(((int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4]))/5)/3)
            #print "CASA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoria[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorVitoriaCasa.append(0)

        if len(self.dicVitoriaCasa[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorJogosCasa.append((int(self.dicVitoriaCasa[teamKey][0])+int(self.dicVitoriaCasa[teamKey][1])+int(self.dicVitoriaCasa[teamKey][2])+int(self.dicVitoriaCasa[teamKey][3])+int(self.dicVitoriaCasa[teamKey][4]))/15)
            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoriaCasa[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorJogosCasa.append(0)


        if self.games2["HomeTeam"][g]==teamKey:
            self.dicKeyNumJogos[str(teamKey)]+=1
            self.dicKeyNumJogosCasa[str(teamKey)]+=1
            #maximo 5
            if(self.games2["Vitoria"][g]=="0"):
                self.dicVitoria[teamKey].insert(4,"3")
                self.dicVitoriaCasa[teamKey].insert(4,"3")
                self.ligaT.contadorPontos[teamKey]+=3
                self.ligaT.contadorPontosCasa[teamKey]+=3
            if(self.games2["Vitoria"][g]=="1"):
                self.ligaT.contadorPontos[teamKey]+=1
                self.ligaT.contadorPontosCasa[teamKey]+=1
                self.dicVitoria[teamKey].insert(4,"1")
                self.dicVitoriaCasa[teamKey].insert(4,"1")
            if(self.games2["Vitoria"][g]=="2"):
                self.dicVitoria[teamKey].insert(4,"0")
                self.dicVitoriaCasa[teamKey].insert(4,"0")

            self.ligaT.contadorGolos[teamKey]+=self.games2["FTHG"][g]+self.games2["FTAG"][g]
            self.ligaT.contadorGolosSofridos[teamKey]+=self.games2["HTHG"][g]+self.games2["HTAG"][g]

    def calculaJogoCasa(self,g,teamKey):

        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorVitoriaCasa.append(((int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4])))/10)
            #print "CASA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoria[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorVitoriaCasa.append(0)
        print ("TEAM ",self.dicVitoriaCasa[teamKey])

        if len(self.dicVitoriaCasa[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorJogosCasa.append((int(self.dicVitoriaCasa[teamKey][0])+int(self.dicVitoriaCasa[teamKey][1])+int(self.dicVitoriaCasa[teamKey][2])+int(self.dicVitoriaCasa[teamKey][3])+int(self.dicVitoriaCasa[teamKey][4]))/10)
            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoriaCasa[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorJogosCasa.append(0)

        if self.games2["HomeTeam"][g]==teamKey:
            self.dicKeyNumJogos[str(teamKey)]+=1
            self.dicKeyNumJogosCasa[str(teamKey)]+=1
            #maximo 5
            if(self.games2["Vitoria"][g]=="0"):
                self.dicVitoria[teamKey].insert(4,"2")
                self.dicVitoriaCasa[teamKey].insert(4,"2")
                self.ligaT.contadorPontos[teamKey]+=3
                self.ligaT.contadorPontosCasa[teamKey]+=3
            if(self.games2["Vitoria"][g]=="1"):
                self.ligaT.contadorPontos[teamKey]+=1
                self.ligaT.contadorPontosCasa[teamKey]+=1
                self.dicVitoria[teamKey].insert(4,"1")
                self.dicVitoriaCasa[teamKey].insert(4,"1")
            if(self.games2["Vitoria"][g]=="2"):
                self.dicVitoria[teamKey].insert(4,"0")
                self.dicVitoriaCasa[teamKey].insert(4,"0")

            self.ligaT.contadorGolos[teamKey]+=self.games2["FTHG"][g]+self.games2["FTAG"][g]
            self.ligaT.contadorGolosSofridos[teamKey]+=self.games2["HTHG"][g]+self.games2["HTAG"][g]




    def calculaJogoFora2(self,g,teamKey):

        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorVitoriaFora.append((int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4]))/15)

            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoria[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorVitoriaFora.append(0)


        if len(self.dicVitoriaFora[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            print ("self.calcula.calculadorJogosFora ",self.dicVitoriaFora[teamKey])
            self.calcula.calculadorJogosFora.append((int(self.dicVitoriaFora[teamKey][0])+int(self.dicVitoriaFora[teamKey][1])+int(self.dicVitoriaFora[teamKey][2])+int(self.dicVitoriaFora[teamKey][3])+int(self.dicVitoriaFora[teamKey][4]))/15)
            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoriaFora[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorJogosFora.append(0)

        if self.games2["AwayTeam"][g]==teamKey:
            #maximo 5
            self.dicKeyNumJogos[str(teamKey)]+=1
            self.dicKeyNumJogosFora[str(teamKey)]+=1
            if(self.games2["Vitoria"][g]=="0"):
                self.dicVitoria[teamKey].insert(4,"0")
                self.dicVitoriaFora[teamKey].insert(4,"0")
            if(self.games2["Vitoria"][g]=="1"):
                self.dicVitoria[teamKey].insert(4,"1")
                self.ligaT.contadorPontos[teamKey]+=1
                self.ligaT.contadorPontosFora[teamKey]+=1
                self.dicVitoriaFora[teamKey].insert(4,"1")
            if(self.games2["Vitoria"][g]=="2"):
                self.dicVitoria[teamKey].insert(4,"3")
                self.ligaT.contadorPontos[teamKey]+=3
                self.ligaT.contadorPontosFora[teamKey]+=3
                self.dicVitoriaFora[teamKey].insert(4,"3")
            self.ligaT.contadorGolos[teamKey]+=self.games2["HTHG"][g]+self.games2["HTAG"][g]
            self.ligaT.contadorGolosSofridos[teamKey]+=self.games2["FTHG"][g]+self.games2["FTAG"][g]

    def calculaJogoFora(self,g,teamKey):

        if len(self.dicVitoria[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            self.calcula.calculadorVitoriaFora.append((int(self.dicVitoria[teamKey][0])+int(self.dicVitoria[teamKey][1])+int(self.dicVitoria[teamKey][2])+int(self.dicVitoria[teamKey][3])+int(self.dicVitoria[teamKey][4]))/10)
            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoria[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorVitoriaFora.append(0)


        if len(self.dicVitoriaFora[teamKey]) ==5:
            #print "EQUIPA ",self.dicNomeByKey[teamKey]
            print ("self.calcula.calculadorJogosFora ",self.dicVitoriaFora[teamKey])
            self.calcula.calculadorJogosFora.append((int(self.dicVitoriaFora[teamKey][0])+int(self.dicVitoriaFora[teamKey][1])+int(self.dicVitoriaFora[teamKey][2])+int(self.dicVitoriaFora[teamKey][3])+int(self.dicVitoriaFora[teamKey][4]))/10)
            #print "FORA"
            #print "Antes: ",self.dicVitoria[teamKey]
            self.dicVitoriaFora[teamKey].pop(0)
            #print "Depois: ",self.dicVitoria[teamKey]
        else:
            self.calcula.calculadorJogosFora.append(0)


        if self.games2["AwayTeam"][g]==teamKey:
            #maximo 5
            self.dicKeyNumJogos[str(teamKey)]+=1
            self.dicKeyNumJogosFora[str(teamKey)]+=1
            if(self.games2["Vitoria"][g]=="0"):
                self.dicVitoria[teamKey].insert(4,"0")
                self.dicVitoriaFora[teamKey].insert(4,"0")
            if(self.games2["Vitoria"][g]=="1"):
                self.dicVitoria[teamKey].insert(4,"1")
                self.dicVitoriaFora[teamKey].insert(4,"1")
                self.ligaT.contadorPontos[teamKey]+=1
                self.ligaT.contadorPontosFora[teamKey]+=1
            if(self.games2["Vitoria"][g]=="2"):
                self.dicVitoria[teamKey].insert(4,"2")
                self.dicVitoriaFora[teamKey].insert(4,"2")
                self.ligaT.contadorPontos[teamKey]+=3
                self.ligaT.contadorPontosFora[teamKey]+=3

            self.ligaT.contadorGolos[teamKey]+=self.games2["HTHG"][g]+self.games2["HTAG"][g]
            self.ligaT.contadorGolosSofridos[teamKey]+=self.games2["FTHG"][g]+self.games2["FTAG"][g]




    def printa(self):
        self.geraClubes()
        self.geraVitoria()
        self.ligaT.contadorGolos
        for g in range(len(self.games2["HomeTeam"].dropna(how='any'))):
            if self.games2["AwayTeam"][g] != "nan":
                keyH=0
                keyA=0

                self.calcula.pontosCalculo(self.ligaT.contadorPontos[self.games2["HomeTeam"][g]],self.ligaT.contadorPontos[self.games2["AwayTeam"][g]],self.dicKeyNumJogos[self.games2["HomeTeam"][g]],self.dicKeyNumJogos[self.games2["AwayTeam"][g]])
                self.arratBTW=self.calcula.jogosEntreEquipas2(self.games2,self.games2["HomeTeam"][g],self.games2["AwayTeam"][g],self.arratBTW,g)
                self.calcula.pontosPorEquipa(self.ligaT.contadorPontosCasa[self.games2["HomeTeam"][g]],self.ligaT.contadorPontosFora[self.games2["AwayTeam"][g]],self.dicKeyNumJogosCasa[self.games2["HomeTeam"][g]],self.dicKeyNumJogosFora[self.games2["AwayTeam"][g]])
                self.calcula.golosCalculo(self.ligaT.contadorGolos[self.games2["HomeTeam"][g]],self.ligaT.contadorGolosSofridos[self.games2["HomeTeam"][g]],self.ligaT.contadorGolos[self.games2["AwayTeam"][g]],self.ligaT.contadorGolosSofridos[self.games2["AwayTeam"][g]],self.dicKeyNumJogos[self.games2["HomeTeam"][g]],self.dicKeyNumJogos[self.games2["AwayTeam"][g]])
                self.calculaJogoCasa2(g,self.games2["HomeTeam"][g])
                self.calculaJogoFora2(g,self.games2["AwayTeam"][g])


    def getValues(self,keyH,keyA):


        print self.calcula.calculadorEntreEquipas
        print self.ligaT.contadorPontosCasa[keyH]
        print self.ligaT.contadorPontosFora[keyA]
        print self.dicKeyNumJogosCasa[keyH]
        print self.dicKeyNumJogosFora[keyA]
        self.calcula.pontosCalculo(self.ligaT.contadorPontos[keyH],self.ligaT.contadorPontos[keyA],self.dicKeyNumJogos[keyH],self.dicKeyNumJogos[keyA])
        self.calcula.pontosPorEquipa(self.ligaT.contadorPontosCasa[keyH],self.ligaT.contadorPontosFora[keyA],self.dicKeyNumJogosCasa[keyH],self.dicKeyNumJogosFora[keyA])

        jCasa=self.calculaJC(keyH)
        jFora=self.calculaJF(keyA)

        pontosT=self.calcula.pontosFinal[len(self.calcula.pontosFinal)-1]
        pontosCasa=self.calcula.pontosFinalCasa_Fora[len(self.calcula.pontosFinalCasa_Fora)-1]

        return[jCasa,jFora,jBTW,pontosT,pontosCasa]
        #print self.calcula.calculadorVitorias

        # d = {'HomeTeam' : pandas.Series(self.games2["HomeTeam"]),
        #  'AwayTeam' : pandas.Series(self.games2["AwayTeam"]),
        #  'team1' : pandas.Series(self.calcula.calculadorVitoriaCasa),
     #       'team2': pandas.Series(self.calcula.calculadorVitoriaFora),
     #       'gameBTW': pandas.Series(self.calcula.calculadorEntreEquipas),
     #       'golo diff':pandas.Series(self.calcula.pontosFinal),
     #       'golo diff2':pandas.Series(self.calcula.pontosFinalCasa_Fora),
     #       'golo diff3':pandas.Series(self.calcula.golosFinal),
     #      'vitoria' : pandas.Series(self.games2["Vitoria"])
        # }
        # df= pandas.DataFrame(d)
        # df.to_csv(self.nomeFicheiro+".csv", sep='\t')
        #print self.ligaT.contadorPontos



