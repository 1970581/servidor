# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# TEST
import pandas as pd

# Extrotura dos CSV's
#
# Key to results data:
# Div = League Division
# Date = Match Date (dd/mm/yy)
# Time = Time of match kick off
# HomeTeam = Home Team
# AwayTeam = Away Team
# FTHG and HG = Full Time Home Team Goals
# FTAG and AG = Full Time Away Team Goals
# FTR and Res = Full Time Result (H=Home Win, D=Draw, A=Away Win)
# HTHG = Half Time Home Team Goals
# HTAG = Half Time Away Team Goals
# HTR = Half Time Result (H=Home Win, D=Draw, A=Away Win)
# .... omitidos
# VER: https://www.football-data.co.uk/notes.txt

# Load dos ficheiros CSV
#diretorio = "c:/sadec/dataset/"
diretorio = ""  #usar o work dir, ou seja o diretorio onde esta

df_2017 = pd.read_csv(diretorio + "2017_2018.csv")
df_2018 = pd.read_csv(diretorio + "2018_2019.csv")
df_2019 = pd.read_csv(diretorio + "2019_2020.csv")
df_2020 = pd.read_csv(diretorio + "2020_2021.csv")
# Nota: Não existe a coluna time no 2 primeiros csv's.

# Juntar os csv's num unico dataframe
frames = [df_2017, df_2018, df_2019, df_2020]
df_all = pd.concat(frames)



# Validar/debug com print.
print("Numero linhas 2017" , len(df_2017))
print("Numero linhas 2018" , len(df_2018))
print("Numero linhas 2019" , len(df_2019))
print("Numero linhas 2020" , len(df_2020))
print("Numero linhas total" , len(df_all))
print(df_all.head(5))


# Criar uma nova dataframe com apenas estas colunas.
df = df_all[['Date','HomeTeam','AwayTeam','FTHG','FTAG','FTR']].copy()
# Refazer o index  https://stackoverflow.com/questions/20490274/how-to-reset-index-in-a-pandas-dataframe
df = df.reset_index(drop=True)

#Adicionar a coluna do historico de vitorias para aquele par de equipas, a zeros
df['historico'] = 0
#Adicionar a coluna de mare de vitorias/derrotas da HomeTeam, a zeros
df['HomeMare'] = 0
#Adicionar a coluna de mare de vitorias/derrotas da AwayTeam, a zeros
df['AwayMare'] = 0
#Adicionar a coluna de UnderOver, com default under
df['UnderOver'] = "under"

# Validar/Debug atraves de prints.
print(df.head(5))
print(df.tail(5))
print("Numero linhas total" , len(df))




# Calculo do Under ou Over 2.5 golos
for myIndex in range(len(df)):
    golos = df.at[myIndex, 'FTHG'] + df.at[myIndex, 'FTAG']
    if golos < 2.5:
        df.at[myIndex, 'UnderOver'] = "under"
    else:
        df.at[myIndex, 'UnderOver'] = "over"
        
    


# Funcao para calcular historico dos jogos com este exacto par de equipas
# Vai adicionando ou subtraido conforme uma das equipas vence ou perde
# Percorre todas as linhas passadas, para obter um valor.
# 0 empatadas ou nunca jogaram
# 1+ home team venceu X vezes mais que a away team
# 1- away team venceu X vezes mais que a home team
def historical_match(data, home, away):
    resultado = 0
    size = len(data)
    # Iterara linha a linha https://stackoverflow.com/questions/16476924/how-to-iterate-over-rows-in-a-dataframe-in-pandas
    for index, row in data.iterrows():
        myHome = row['HomeTeam']
        myAway = row['AwayTeam']
        if ((myHome == home and myAway == away) or (myHome == away and myAway == home)):
            htr = row['FTR']
            if (htr == 'A'):
                resultado -= 1
            elif (htr == 'H'):
                resultado += 1
    return resultado

# Validar/DEbug.
#df2 = df[:]
#x = historical_match(df2, 'Arsenal', 'Leicester' )
#print('virotias n:', x)
#x = historical_match(df2, 'Leicester', 'Arsenal' )
#print('virotias n:', x)

# Preencher a coluna 'historico' com a historia de vitorias/derotas das equipas
# Refaz os calculos independemente para cada linha, chamando uma funcao.
# Super lento, 30 segundos, O(N2) com 1000 valores.

for myIndex in reversed(range(len(df))):
    home = df.at[myIndex,'HomeTeam']
    away = df.at[myIndex,'AwayTeam']
    df_aux = df[:myIndex]  # passamos apenas as linhas acima
    wins = historical_match(df_aux, home, away ) 
    df.at[myIndex,'historico'] = wins

# Validar/DEbug.
print(df.head(5))
print(df.tail(5))





# Calculo de vitorias ou derrotas consecutivas. AKA mares de vitorias/derotas

# Obter o nome de todas as equipas.
column_values = df[["HomeTeam", "AwayTeam"]].values.ravel()
unique_teams =  pd.unique(column_values)
print(unique_teams)

# Funcao para preencher o ao longo do dataframe a coluna correspondente as
# vitorias ou derrotas desde que perdeu ou empatou.
# Retorna o dataframe preenchido.
def preencher_mares_vitorias(data, equipa): 
    mare = 0       
    for myIndex in range(len(df)):
        
        # Caso HomeTeam
        if data.at[myIndex,'HomeTeam'] == equipa:
            data.at[myIndex,'HomeMare'] = mare
            
            if data.at[myIndex,'FTR'] == 'H':  # Venceu
                if mare < 0:                
                    mare = 1
                else:
                    mare += 1            
            if data.at[myIndex,'FTR'] == 'A':  # Perdeu
                if mare > 0:                
                    mare = -1
                else:
                    mare -= 1            
            if data.at[myIndex,'FTR'] == 'D':   # Empate
                mare = 0
            
        # Caso AwayTeam    
        if data.at[myIndex,'AwayTeam'] == equipa:
            data.at[myIndex,'AwayMare'] = mare
            
            if data.at[myIndex,'FTR'] == 'H':  # Perdeu
                if mare > 0:                
                    mare = -1
                else:
                    mare -= 1            
            if data.at[myIndex,'FTR'] == 'A':  # Venceu
                if mare < 0:                
                    mare = 1
                else:
                    mare += 1            
            if data.at[myIndex,'FTR'] == 'D':   # Empate
                mare = 0                
    return data
        

for aTeam in unique_teams:
    df = preencher_mares_vitorias(df, aTeam)
    
# Validar/DEbug.
print(df.head(5))
print(df.tail(5))

# Escrever o csv no disco. From: https://towardsdatascience.com/how-to-export-pandas-dataframe-to-csv-2038e43d9c03
df.to_csv('dataset.csv', index=False , encoding='utf-8')

#print(myIndex, wins)
#for index, row in df2.iterrows():
#    print(row['Date'], row['HomeTeam'])

#df[1][1]

