# -*- coding: utf-8 -*-
"""
Created on Tue Dec 29 14:51:22 2020

@author: Legion
"""

# Servidor de ML para previsao de futebol.
# https://www.youtube.com/watch?v=i3RMlrx4ol4


# pip install flask : para poder usar a libraria Flask
from flask import Flask, request, abort

# TIVEMOS DE ADICIONAR CORS por causa do FIREFOX!!!"!"!!!"!"!
# https://stackoverflow.com/questions/25594893/how-to-enable-cors-in-flask
# Tem de ser instalado pelo command prompt do anaconda. https://anaconda.org/conda-forge/flask-cors
# conda install -c conda-forge flask-cors 
from flask_cors import CORS, cross_origin

# Import da classe de utilitarios para equipas
from teamutil import TeamUtil

# imports de ML
import pickle  # Necessario para fazer load do modelo.
from sklearn.svm import SVC # Modelo svm.
import pandas as pd

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')    # ,methods=['GET']
@cross_origin()
def home():
    # Funcao de resposta ao GET, http://127.0.0.1:5000/ que dadas duas equipas diz o resultado
    # Uzar o link http://127.0.0.1:5000/?home=Sheffield United&away=Huddersfield
    hasAway = False
    hasHome = False
    #name = request.form('home', None)   # exemplo para form
    
    # Obter of parametros url encoded
    req_away = request.args.get('away')
    if req_away is None:
        hasAway = False
    else:
        hasAway = True
    req_home = request.args.get('home')
    if req_home is None:
        hasHome = False
    else:
        hasHome = True
        
    # Debug
    texto = "Home: " + str(req_home or 'None') + " " + " Away: " + str(req_away or 'None')
    print("GET: " , texto)
    
    # Validar nome das equipas
    utils = TeamUtil()
    # utils.testPrint()  #3 Debug
    if (hasHome == False or hasAway == False):
        abort(400)
        print("Abort devido a parametros None")        
    if ( utils.isTeam(req_home) == False or utils.isTeam(req_away) == False):
        abort(400)
        print("Abort devido a nome de equipas inexistentes")        
    
    # Obter os numeros das equipas
    num_home = utils.getTeamNumber(req_home) 
    num_away = utils.getTeamNumber(req_away)
    print(req_home , str(num_home) , req_away , str(num_away))
    
    ### Previsao ###
    
    # Load dos modelos
    svclassifier_underover = pickle.load( open( "svclassifier_underover.p", "rb" ) )
    nn_modelo_resultado = pickle.load( open( "nn_modelo_resultado.p", "rb" ) )
    
    
    # Criacao do array a prever 'nHomeTeam','nAwayTeam','historico','HomeMare','AwayMare',
    my_csv = pd.read_csv("dataset.csv")
    historico = utils.getHistorico(req_home, req_away, my_csv)    
    homemare = utils.getMare(req_home, my_csv)
    awaymare = utils.getMare(req_away, my_csv)    
    dados_equipas = pd.DataFrame([[num_home, num_away, historico, homemare, awaymare]], dtype='int32')    
    print("Os dados a alimentar ao modelo:")
    print(dados_equipas)
    
    
    # Prever    
    # UnderOver devolve -1 under, 1 over
    # Resultado devolve 1 vitoria casa, -1 vitoria away, 0 empate
    my_underover = svclassifier_underover.predict(dados_equipas)
    my_resultado = nn_modelo_resultado.predict(dados_equipas)
    
    print("Resultado da previsao resultado:",my_resultado)
    print("Resultado da previsao underOver:",my_underover)
    
    # Output dos resultados.
    texto_resultado = "ERRO"
    if(my_resultado[0] == -1):
        texto_resultado = "Vitoria da equipa visitante, " + req_away + " "
    if(my_resultado[0] == 1):
        texto_resultado = "Vitoria da equipa da casa, " + req_home + " "
    if(my_resultado[0] == 0):
        texto_resultado = "Empate "        
    texto_underOver = "ERRO"
    if(my_underover[0] == -1):
        texto_underOver = "com Under 2.5 (menos de 2.5 golos)."
    if(my_underover[0] == 1):
        texto_underOver = "com Over 2.5 (mais de 2.5 golos)."
    
    resposta = texto_resultado + texto_underOver

    return resposta

if __name__ == "__main__":
    #app.run(debug=True)    # usar true para correr fora do Spyder
    app.run(debug=False)    # usar false para correr no Spyder
    
    
    
    