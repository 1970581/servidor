# -*- coding: utf-8 -*-
"""
Created on Tue Dec 29 16:34:46 2020

@author: Legion
"""

class TeamUtil:
    
    
        
    # Teste que a classe esta a funcionar.
    def testPrint(self):
        print("Print test. TeamUutil OK.")
        print(equipas)
        
    # Verifica se o nome e uma equipa
    def isTeam(self, teamname):
        if(teamname in equipas):
            return True
        else:
            return False
        return False

    #    Devolve o numero assignado a equipa baseado no array acima.
    def getTeamNumber(self, teamname):
        return equipas.index(teamname)
    
    # Determina o valor do historico, dao as duas equipas e o dataset.
    def getHistorico(self, home_teamname, away_teamname, csv):
        resposta = 0
        for myIndex in reversed(range(len(csv))):
            home = csv.at[myIndex,'HomeTeam']
            away = csv.at[myIndex,'AwayTeam']
            if (home_teamname == home and away_teamname == away):
                resposta = csv.at[myIndex,'historico']
                if(csv.at[myIndex,'FTR'] == 'H'):
                    resposta = resposta +1
                if(csv.at[myIndex,'FTR'] == 'A'):
                    resposta = resposta - 1
                #print("Found", resposta)
                break
            if (home_teamname == away and away_teamname == home):
                resposta = csv.at[myIndex,'historico']
                if(csv.at[myIndex,'FTR'] == 'H'):
                    resposta = resposta - 1
                if(csv.at[myIndex,'FTR'] == 'A'):
                    resposta = resposta + 1
                #print("Found", resposta)
                break            
        return resposta
    
    def getMare(self, teamname, csv):
        mare = 0
        for myIndex in reversed(range(len(csv))):
            home = csv.at[myIndex,'HomeTeam']
            away = csv.at[myIndex,'AwayTeam']
            
            if (teamname == home or teamname == away):                
                if (teamname == home):
                    mare = csv.at[myIndex,'HomeMare']
                    if csv.at[myIndex,'FTR'] == 'H':  # Venceu
                        if mare < 0:                
                            mare = 1
                        else:
                            mare += 1            
                    if csv.at[myIndex,'FTR'] == 'A':  # Perdeu
                        if mare > 0:                
                            mare = -1
                        else:
                            mare -= 1            
                    if csv.at[myIndex,'FTR'] == 'D':   # Empate
                        mare = 0
                else:
                    mare = csv.at[myIndex,'AwayMare']
                    if csv.at[myIndex,'FTR'] == 'H':  # Perdeu
                        if mare > 0:                
                            mare = -1
                        else:
                            mare -= 1            
                    if csv.at[myIndex,'FTR'] == 'A':  # Venceu
                        if mare < 0:                
                            mare = 1
                        else:
                            mare += 1            
                    if csv.at[myIndex,'FTR'] == 'D':   # Empate
                         mare = 0      
                
                break
        return mare             
            
            
   

        
        
equipas = ['Arsenal', 'Leicester', 'Brighton', 'Man City', 'Chelsea', 'Burnley',
               'Crystal Palace', 'Huddersfield', 'Everton', 'Stoke', 'Southampton', 'Swansea',
               'Watford', 'Liverpool', 'West Brom', 'Bournemouth', 'Man United', 'West Ham',
               'Newcastle', 'Tottenham', 'Cardiff', 'Fulham', 'Wolves', 'Norwich',
               'Sheffield United', 'Aston Villa', 'Leeds']        

        