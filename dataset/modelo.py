# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 17:35:38 2020

@author: Legion
"""
import pandas as pd
from teamutil import TeamUtil
import pickle



my_csv = pd.read_csv("dataset.csv")
df = pd.read_csv("dataset.csv")



# Drop de colunas
df = df.drop(labels=['Date'], axis=1)
df = df.drop(labels=['FTHG'], axis=1)
df = df.drop(labels=['FTAG'], axis=1)

# PYthon nao suporta classes, e os metodos de ML apenas aceitam numeros.
# Pelo que temos de transformar os string em numeros.
# 
# Metodo automatico mas com menos controlo, COMENTADO
# https://stackoverflow.com/questions/51552438/categorical-string-features-in-sklearn-for-10cv-svm-regression
#from sklearn.preprocessing import LabelEncoder
#df = df.apply(LabelEncoder().fit_transform)  
#
# METODO manual com mais controlo

# Transformacao coluna FTR -  Fulltime result : H home, A away, D draw
for myIndex in range(len(df)):
    if df.at[myIndex, 'FTR'] == 'H':
        df.at[myIndex, 'FTR'] = 1
    if df.at[myIndex, 'FTR'] == 'A':
        df.at[myIndex, 'FTR'] = -1
    if  df.at[myIndex, 'FTR'] == 'D':
        df.at[myIndex, 'FTR'] = 0
df

# Transformacao coluna UnderOver 
for myIndex in range(len(df)):
    if df.at[myIndex, 'UnderOver'] == 'over':
        df.at[myIndex, 'UnderOver'] = 1
    else:
        df.at[myIndex, 'UnderOver'] = -1



# Criar colunas com numeros de equipas.
df['nAwayTeam'] = 0
df['nHomeTeam'] = 0
utils = TeamUtil()
for myIndex in range(len(df)):
    num_home = utils.getTeamNumber(df.at[myIndex, 'HomeTeam']) 
    num_away = utils.getTeamNumber(df.at[myIndex, 'AwayTeam']) 
    df.at[myIndex, 'nHomeTeam'] = num_home
    df.at[myIndex, 'nAwayTeam'] = num_away

    
# Criar uma nova dataframe com apenas estas colunas.
df = df[['nHomeTeam','nAwayTeam','historico','HomeMare','AwayMare','FTR','UnderOver']].copy()

# Colocar todas as colunas com typo int32 para evitar ERRO de ValueError: Unknown label type: 'unknown'
# Algumas tinham strings e portanto ainda sao do tipo Object
df = df.astype('int32')
df.dtypes     # df.dtypes - retorna o typo dos dados no dataframe.
    
###  I  UNDER/OVER

# Divisao TREINO/TESTE 

# MEtodo de divisao treino/teste que e usado normalmalmente para nao Time Series
#from sklearn.model_selection import train_test_split
#train, test = train_test_split(df, test_size=0.2)


# MEtodo de divisao treino/teste para TIMESERIES
# SPLIT Timeseries:
# https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html
# https://medium.com/keita-starts-data-science/time-series-split-with-scikit-learn-74f5be38489e

#Exemplo de:
# https://medium.com/@dcamarena0229/time-series-split-with-scikit-learn-de7ec17d69cd
from sklearn.model_selection import TimeSeriesSplit

# n-splits determine the number folds. N-1 in this case we split our # data into two sets. (3-1)
tss = TimeSeriesSplit(n_splits = 3)

X = df.drop(labels=['FTR','UnderOver'], axis=1)
y = df['UnderOver']

# This was the trickiest part as a newbie. Straight from the docs
# If you only have experience with CV splits this way
# of making the splits might seem foreign. Fret not.
for train_index, test_index in tss.split(X):
    X_train, X_test = X.iloc[train_index, :], X.iloc[test_index,:]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    

# TREINO #

# https://stackabuse.com/implementing-svm-and-kernel-svm-with-pythons-scikit-learn/
    
from sklearn.svm import SVC

svclassifier_underover = SVC(kernel='poly', degree=8)
svclassifier_underover.fit(X_train, y_train)

y_pred = svclassifier_underover.predict(X_test) # Prever

from sklearn.metrics import classification_report, confusion_matrix
print("Matriz UNDER OVER")
print(confusion_matrix(y_test,y_pred))
print("Metricas")
print(classification_report(y_test,y_pred))


###  II  Modelo para vitoria do jogo

y2 = df['FTR']   # Coluna do resultado

for train_index, test_index in tss.split(X):
    X_train, X_test = X.iloc[train_index, :], X.iloc[test_index,:]
    y2_train, y2_test = y2.iloc[train_index], y2.iloc[test_index]
   
svclassifier_resultado = SVC(kernel='poly', degree=8)
svclassifier_resultado.fit(X_train, y2_train)

y2_pred = svclassifier_resultado.predict(X_test) # Prever

#from sklearn.metrics import classification_report, confusion_matrix
print("Matriz RESULTADO")
print(confusion_matrix(y2_test,y2_pred))
print("Metricas")
print(classification_report(y2_test,y2_pred))


from sklearn.neural_network import MLPClassifier
nn_modelo_resultado = MLPClassifier(solver='lbfgs',activation='relu',learning_rate="adaptive",alpha=1e-7,hidden_layer_sizes=(17,), random_state=1,max_iter=350)
nn_modelo_resultado.fit(X_train,y2_train)

# Testar o modelo com o set de teste
y2_pred = nn_modelo_resultado.predict(X_test)

print("Matriz RESULTADO")
print(confusion_matrix(y2_test,y2_pred))
print("Metricas")
print(classification_report(y2_test,y2_pred))



### Salvar os ficheiros dos modelos.
#
# Esquema do alimentar para prever.
# 'nHomeTeam','nAwayTeam','historico','HomeMare','AwayMare',
#
# UnderOver devolve -1 under, 1 over
# Resultado devolve 1 vitoria casa, -1 vitoria away, 0 empate
pickle.dump( svclassifier_underover, open( "svclassifier_underover.p", "wb" ) )
pickle.dump( nn_modelo_resultado, open( "nn_modelo_resultado.p", "wb" ) )


print( "Modelos guardados, esperamos nos.")