# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 16:05:47 2020

@author: Legion
"""

# TESTE DO TEAM UTILS.

from teamutil import TeamUtil
import pandas as pd


df = pd.read_csv("dataset.csv")
utils = TeamUtil()
home = "Sheffield United"
away = "Huddersfield"
home = "West Ham"
away = "Aston Villa"


utils.testPrint() 
print("Is a team:",utils.isTeam(home))
print("Team Number",utils.getTeamNumber(home) )
print("Is a team:",utils.isTeam(away))
print("Team Number",utils.getTeamNumber(away) )


historico = utils.getHistorico(home, away, df)
print("Historico:", historico)

mare = utils.getMare(home, df)
print("Mare:", mare)